/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// CronjobMonitorSpec defines the desired state of CronjobMonitor
type CronjobMonitorSpec struct {
	Name                     string `json:"name,omitempty"`
	UnscheduledCheckInterval int    `json:"unscheduled_check_interval,omitempty"`
}

// CronjobMonitorStatus defines the observed state of CronjobMonitor
type CronjobMonitorStatus struct {
	Name string `json:"name,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// CronjobMonitor is the Schema for the cronjobmonitors API
type CronjobMonitor struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   CronjobMonitorSpec   `json:"spec,omitempty"`
	Status CronjobMonitorStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// CronjobMonitorList contains a list of CronjobMonitor
type CronjobMonitorList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []CronjobMonitor `json:"items"`
}

func init() {
	SchemeBuilder.Register(&CronjobMonitor{}, &CronjobMonitorList{})
}
